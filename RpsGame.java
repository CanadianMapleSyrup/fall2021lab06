package fall2021lab06;

import java.util.Random;

/**
 * @author Nolan Ganz
 */
public class RpsGame {
    private int wins;
    private int losses;
    private int ties;
    private Random rand;
    private static final String[] choices = {"rock","paper","scissors"};

    public RpsGame() {
        this.wins = 0;
        this.losses = 0;
        this.ties = 0;
        this.rand = new Random();
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public int getTies() {
        return ties;
    }

    public String playRound(String playerC) {
        String computerC = choices[this.rand.nextInt(3)];

        //Player Win Conditions
        boolean pRWin = playerC.equals(choices[0]) && computerC.equals(choices[2]);
        boolean pPWin = playerC.equals(choices[1]) && computerC.equals(choices[0]);
        boolean pSWin = playerC.equals(choices[2]) && computerC.equals(choices[1]);
        boolean pWinCondition = (pRWin || pPWin || pSWin);

        //Player Lose Conditions
        boolean pRLose = playerC.equals(choices[0]) && computerC.equals(choices[1]);
        boolean pPLose = playerC.equals(choices[1]) && computerC.equals(choices[2]);
        boolean pSLose = playerC.equals(choices[2]) && computerC.equals(choices[0]);
        boolean pLoseCondition = (pRLose || pPLose || pSLose);

        if ( playerC.equals(computerC)) {
            this.ties++;
            return "Computer chose " + computerC + " and it tied.";
        }
        else if (pLoseCondition) {
            this.losses++;
            return "Computer chose " + computerC + " and it won.";
        }
        else if (pWinCondition) {
            this.wins++;
            return "Computer chose " + computerC + " and it lost.";
        }
        else {
            return "Some input was invalid and thus an error occurred";
        }
    }

    
}
